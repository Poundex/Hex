package net.poundex.hex.javafx;

import lombok.RequiredArgsConstructor;
import net.poundex.hex.context.CalculatorContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class InputHandler {
	
	private final CalculatorContext calculatorContext;
	
	private final List<Runnable> listeners = new ArrayList<>();
	
	public void handleInput(String input) {
		calculatorContext.parse(input);
		listeners.forEach(Runnable::run);
	}
	
	public void addListener(Runnable runnable) {
		listeners.add(runnable);
	}
}
