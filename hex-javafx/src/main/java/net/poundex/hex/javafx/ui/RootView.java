package net.poundex.hex.javafx.ui;

import jakarta.annotation.PostConstruct;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RootView extends BorderPane {
	private final StackView stackView;
	private final InputView inputView;
	private final StatusView statusView;
	
	@PostConstruct
	void init() {
		setCenter(stackView);
		setBottom(new VBox(inputView, statusView));
	}
}
