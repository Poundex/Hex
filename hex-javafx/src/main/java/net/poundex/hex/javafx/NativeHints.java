package net.poundex.hex.javafx;

import org.springframework.aot.hint.MemberCategory;
import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;

import java.util.stream.Stream;

public class NativeHints implements RuntimeHintsRegistrar {
	@Override
	public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
		Stream.of(
				HexJavaFxApplication.Launcher.class)
			.forEach(klass ->
				hints.reflection().registerType(klass,
					MemberCategory.INTROSPECT_PUBLIC_CONSTRUCTORS,
					MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS));

		// TODO
//		hints.resources().registerResource(new ClassPathResource("version.txt"));
	}
}
