package net.poundex.hex.javafx.ui;

import jakarta.annotation.PostConstruct;
import javafx.scene.control.Label;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.javafx.InputHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class StatusView extends Label {
    private final InputHandler inputHandler;
    private final CalculatorContext calculatorContext;
    
    @PostConstruct
    void init() {
        inputHandler.addListener(this::updateStatus);
    }

    private void updateStatus() {
        setText(calculatorContext.getStatus().message());
    }
}
