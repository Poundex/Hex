package net.poundex.hex.javafx;


import io.vavr.control.Try;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import net.poundex.hex.engine.HexContextConfiguration;
import net.poundex.hex.javafx.ui.RootView;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportRuntimeHints;
import org.springframework.core.io.ClassPathResource;

import java.util.concurrent.CountDownLatch;

@SpringBootApplication
@ImportRuntimeHints(NativeHints.class)
@Import(HexContextConfiguration.class)
@Slf4j
public class HexJavaFxApplication {

	private static final CountDownLatch latch = new CountDownLatch(1);
	private static Stage primaryStage;

	public static void main(String[] args) {
		new Thread(() -> Launcher.launch(Launcher.class)).start();
		Try.run(latch::await)
			.andThenTry(() -> SpringApplication.run(HexJavaFxApplication.class, args))
			.onFailure(SpringApplication.AbandonedRunException.class, ignored -> Platform.exit())
			.get();
	}

	public static class Launcher extends Application {
		@Override
		public void start(Stage primaryStage) {
			HexJavaFxApplication.primaryStage = primaryStage;
			latch.countDown();
		}
	}

	@Bean
	public ApplicationListener<ApplicationReadyEvent> listener(RootView root) {
		return ignored -> Platform.runLater(() -> {
			primaryStage.setTitle("Hex");
			primaryStage.getIcons().add(new Image(
				Try.of(() -> new ClassPathResource("/cheese_small.png").getInputStream()).get(), 
				64, 
				64, 
				true, 
				true));
			Scene scene = new Scene(root, 300, 400);
			scene.getStylesheets().add(Try.of(() -> new ClassPathResource("hex.css").getURL().toExternalForm()).get());
			primaryStage.setScene(scene);
			primaryStage.sizeToScene();

			Rectangle2D screenBounds = Screen.getPrimary().getBounds();
//			log.atInfo().log("Screen bounds: {}x{}", screenBounds.getWidth(), screenBounds.getHeight());
//			log.atInfo().log("Scene size: {}x{}", scene.getWidth(), scene.getHeight());
			double x = (screenBounds.getWidth() - scene.getWidth()) / 2;
			double y = ((screenBounds.getHeight() - scene.getHeight()) / 2) + scene.getHeight();
			primaryStage.setX(x);
			primaryStage.setY(y);

//			log.atInfo().log("Stage size: {}x{}", primaryStage.getWidth(), primaryStage.getHeight());
//			log.atInfo().log("Stage pos'n: ({},{})", primaryStage.getX(), primaryStage.getY());

			primaryStage.show();
			primaryStage.setX(x);
			primaryStage.setY(y);
		});
	}
}
