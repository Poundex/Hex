package net.poundex.hex.javafx.ui;

import jakarta.annotation.PostConstruct;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.javafx.InputHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class InputView extends BorderPane {
	
	private final InputHandler inputHandler;

	private TextField textField;
	private Button button;

	@PostConstruct
	void init() {
		textField = new TextField();
		textField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER)
				onButtonClicked(null);
		});

		button = new Button("⏎");

		button.setOnAction(this::onButtonClicked);
		button.setFocusTraversable(false);

		setCenter(textField);
		setRight(button);
	}

	private void onButtonClicked(ActionEvent actionEvent) {
		inputHandler.handleInput(textField.getText());
		textField.setText("");
	}
}
