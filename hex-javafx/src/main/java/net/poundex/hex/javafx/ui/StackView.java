package net.poundex.hex.javafx.ui;

import jakarta.annotation.PostConstruct;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.javafx.InputHandler;
import net.poundex.hex.stack.StackElement;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Slf4j
class StackView extends ScrollPane {

	private final InputHandler inputHandler;
	private final CalculatorContext calculatorContext;

	private boolean handlingInput = false;
	private VBox vBox;

	@PostConstruct
	void init() {
		setFitToHeight(true);
		setFitToWidth(true);

		vBox = new VBox();
		vBox.setFillWidth(true);
		vBox.setSpacing(1d);
		vBox.getChildren().addListener((ListChangeListener<Node>) c -> setVvalue(getVmax()));

		AnchorPane anchorPane = new AnchorPane(vBox);
		AnchorPane.setBottomAnchor(vBox, 0d);
		AnchorPane.setLeftAnchor(vBox, 0d);
		AnchorPane.setRightAnchor(vBox, 0d);
		setContent(anchorPane);

		inputHandler.addListener(this::afterInputHandled);
		vvalueProperty().addListener((observable, oldValue, newValue) -> {
			if ( ! newValue.equals(1d) && handlingInput)
				setVvalue(getVmax());
			handlingInput = false;
		});
	}

	private static class StackElementCell extends BorderPane {
		private final Label indexLabel = new Label();
		private final TextField valueLabel = new TextField();

		StackElementCell(int idx, String value) {
			super();
			getStyleClass().add("stack-element-cell");
			getStyleClass().add(idx % 2 == 0 ? "even" : "odd");
			indexLabel.setDisable(true); // TODO: Use CSS instead
			setPadding(new Insets(2, 2, 2, 2));

			indexLabel.setText(idx + ":");
			valueLabel.setEditable(false);
			valueLabel.setText(value);
			valueLabel.setAlignment(Pos.CENTER_RIGHT);
			valueLabel.getStyleClass().add("value-display");
			valueLabel.setMaxWidth(Double.MAX_VALUE);
			MenuItem copy = new MenuItem("Copy");
			copy.setOnAction(event -> {
				ClipboardContent content = new ClipboardContent();
				content.putString(value);
				Clipboard.getSystemClipboard().setContent(content);
				valueLabel.deselect();
			});
			valueLabel.setContextMenu(new ContextMenu(copy));
			valueLabel.setOnContextMenuRequested(event -> valueLabel.selectAll());
			setLeft(indexLabel);
			setCenter(valueLabel);
		}
	}

	private void afterInputHandled() {
		setVvalue(getVmax());
		handlingInput = true;
		Map<Integer, StackElement> view = calculatorContext.getStack().getView();
		vBox.getChildren().setAll(
				IntStream.range(0, view.size())
						.boxed()
						.map(i -> view.size() - i - 1)
						.map(i -> new StackElementCell(i, view.get(i).getDisplay(calculatorContext)))
						.toList());
	}
}
