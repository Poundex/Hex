package net.poundex.hex.cli.session

import net.poundex.hex.function.FunctionRegistry
import net.poundex.hex.symbol.SymbolManager
import org.jline.reader.Candidate
import spock.lang.Specification
import spock.lang.Subject

class InteractiveCompleterSpec extends Specification {
	
	SymbolManager symbolManager = Stub()
	FunctionRegistry functionRegistry = Stub()
	
	@Subject
	InteractiveCompleter completer = new InteractiveCompleter(symbolManager, functionRegistry)
	
	void "Completes with symbols and functions"() {
		given:
		List<Candidate> outList = []
		
		and:
		symbolManager.getNames() >> ["var1", "var2"].toSet()
		functionRegistry.getNames() >> ["op1", "op2"].toSet()
		
		when:
		completer.complete(null, null, outList)
		
		then:
		outList*.value().containsAll([
		        '$var1', '$var2', "op1", "op2"
		])
	}
}
