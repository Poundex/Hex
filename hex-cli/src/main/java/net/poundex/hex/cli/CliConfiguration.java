package net.poundex.hex.cli;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;
import picocli.CommandLine;

import java.nio.charset.StandardCharsets;

@Configuration
@Slf4j
class CliConfiguration {

	@Bean
	public CommandLine commandLine(ApplicationArguments arguments) {
		CommandLine commandLine = new CommandLine(RunOptions.class);
		commandLine.setCommandName("hex");
		commandLine.getCommandSpec().exitCodeOnUsageHelp(CommandLine.ExitCode.USAGE);
		commandLine.getCommandSpec().exitCodeOnVersionHelp(CommandLine.ExitCode.USAGE);
		try {
			commandLine.parseArgs(arguments.getSourceArgs());
		} catch (CommandLine.PicocliException ignored) { }
		return commandLine;
	}
	
	@Bean
	public RunOptions runOptions(CommandLine commandLine) {
		return commandLine.getCommand();
	}

	static class VersionProvider implements CommandLine.IVersionProvider {

		@Override
		public String[] getVersion() throws Exception {
			return new String[] { 
				StreamUtils.copyToString(
					new ClassPathResource("version.txt").getInputStream(), 
					StandardCharsets.UTF_8) };
		}
	}
}
