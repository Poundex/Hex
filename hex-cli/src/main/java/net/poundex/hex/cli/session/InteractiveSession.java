package net.poundex.hex.cli.session;

import lombok.RequiredArgsConstructor;
import net.poundex.hex.cli.RunOptions;
import net.poundex.hex.cli.renderer.RendererFacade;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.StackElement;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.terminal.Size;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.utils.Display;
import org.jline.utils.InfoCmp;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@Component
class InteractiveSession implements CliSession {

	private final CalculatorContext calculatorContext;
	private final RunOptions runOptions;
	private final RendererFacade renderer;
	private final InteractiveCompleter completer;
	
	@Override
	public void start() {
		try(Terminal terminal = TerminalBuilder.builder()
			.build()) {
			
			LineReader lineReader = LineReaderBuilder.builder()
				.terminal(terminal)
				.completer(completer)
				.build();

			Display display = new Display(terminal, false);
			display.resize(runOptions.getDisplayStackSize(), terminal.getSize().getColumns());
			
			new Handler(terminal, lineReader, display).start();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@RequiredArgsConstructor
	private class Handler {
		private final Terminal terminal;
		private final LineReader lineReader;
		private final Display display;

		public void start() {
			while (true) {
				try {
					showStackAndReadInput();
				} catch (EndOfFileException | UserInterruptException ignored) {
					break;
				}
			}
		}

		private void showStackAndReadInput() {
			display.updateAnsi(getStackLines(),
				new Size(terminal.getSize().getColumns(), runOptions.getDisplayStackSize())
					.cursorPos(runOptions.getDisplayStackSize(), 0));

			calculatorContext.parse(lineReader.readLine(
				renderer.renderMessage(calculatorContext.getStatus()) + ">  "));

			terminal.puts(InfoCmp.Capability.cursor_up);
			terminal.puts(InfoCmp.Capability.delete_line);
		}

		private List<String> getStackLines() {
			Map<Integer, StackElement> view = calculatorContext.getStack().getView();
			return IntStream.range(0, runOptions.getDisplayStackSize())
				.boxed()
				.map(i -> runOptions.getDisplayStackSize() - i - 1)
				.map(i -> renderer.renderStackElement(i, Optional.ofNullable(view.get(i))
					.map(se -> se.getDisplay(calculatorContext))
					.orElse(" ")))
				.map(s -> s + "\n")
				.toList();
		}
	}
}
