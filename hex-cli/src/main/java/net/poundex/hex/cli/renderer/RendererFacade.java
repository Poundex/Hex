package net.poundex.hex.cli.renderer;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.cli.RunOptions;
import net.poundex.hex.stack.operation.Status;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class RendererFacade implements Renderer {
	
	private final RunOptions runOptions;
	
	private final PlainRenderer plainRenderer;
	private final ColourRenderer colourRenderer;
	
	private Renderer renderer;
	
	@PostConstruct
	void init() {
		renderer = Optional.ofNullable(runOptions.isColour())
				.orElseGet(CommandLine.Help.Ansi.AUTO::enabled) 
				? colourRenderer 
				: plainRenderer;
	}

	@Override
	public String renderStackElement(int idx, String string) {
		return renderer.renderStackElement(idx, string);
	}

	@Override
	public String renderText(String string) {
		return renderer.renderText(string);
	}

	@Override
	public String renderInfo(String string) {
		return renderer.renderInfo(string);
	}

	@Override
	public String renderBadOperation(String string) {
		return renderer.renderBadOperation(string);
	}

	@Override
	public String renderError(String string) {
		return renderer.renderError(string);
	}
	
	public String renderMessage(Status status) {
		return switch (status.messageType()) {
			case NONE -> renderer.renderText(status.message());
			case INFO -> renderer.renderInfo(status.message());
			case BAD_OPERATION -> renderer.renderBadOperation(status.message());
			case ERROR -> renderer.renderError(status.message());
		};
	}
}
