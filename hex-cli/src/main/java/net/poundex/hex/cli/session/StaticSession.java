package net.poundex.hex.cli.session;

import lombok.RequiredArgsConstructor;
import net.poundex.hex.cli.RunOptions;
import net.poundex.hex.cli.console.Output;
import net.poundex.hex.cli.renderer.RendererFacade;
import net.poundex.hex.context.CalculatorContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Component
class StaticSession implements CliSession {
	
	private final RunOptions runOptions;
	private final CalculatorContext calculatorContext;
	private final Output output;
	private final RendererFacade renderer;
	
	@Override
	public void start() {
		Arrays.stream(runOptions.getInput()).forEach(calculatorContext::parse);
	
		String message = renderer.renderMessage(calculatorContext.getStatus());
		if(StringUtils.hasText(message))
			output.err(message);
	
		Optional.ofNullable(calculatorContext.getStack().peek())
				.map(it -> it.getDisplay(calculatorContext))
				.ifPresent(output::out);
	}
}
