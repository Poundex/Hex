package net.poundex.hex.cli.renderer;

public interface Renderer {
	String renderStackElement(int idx, String string);

	String renderText(String string);
	String renderInfo(String string);
	String renderBadOperation(String string);
	String renderError(String string);
}
