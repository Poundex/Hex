package net.poundex.hex.cli;

import org.jline.terminal.impl.exec.ExecTerminalProvider;
import org.jline.terminal.impl.jansi.JansiTerminalProvider;
import org.jline.terminal.impl.jna.JnaTerminalProvider;
import org.springframework.aot.hint.MemberCategory;
import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;
import org.springframework.core.io.ClassPathResource;

import java.util.stream.Stream;

public class NativeHints implements RuntimeHintsRegistrar {
	@Override
	public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
		Stream.of(
				JansiTerminalProvider.class,
				JnaTerminalProvider.class,
				ExecTerminalProvider.class)
			.forEach(klass ->
				hints.reflection().registerType(klass,
					MemberCategory.INTROSPECT_PUBLIC_CONSTRUCTORS,
					MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS));
		
		hints.resources().registerResource(new ClassPathResource("version.txt"));
	}
}
