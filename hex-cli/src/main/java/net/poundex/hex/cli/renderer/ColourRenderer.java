package net.poundex.hex.cli.renderer;

import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.springframework.stereotype.Component;

@Component
class ColourRenderer implements Renderer {

	@Override
	public String renderStackElement(int idx, String string) {
		return new AttributedStringBuilder()
				.append(String.format("%s: ", idx), AttributedStyle.DEFAULT.foreground(AttributedStyle.CYAN))
				.append(string, idx == 0 ? AttributedStyle.BOLD : AttributedStyle.DEFAULT)
				.toAnsi();
	}

	@Override
	public String renderText(String string) {
		return string;
	}

	@Override
	public String renderInfo(String string) {
		return new AttributedStringBuilder()
				.append(string, AttributedStyle.DEFAULT.foreground(AttributedStyle.BLUE))
				.toAnsi();
	}

	@Override
	public String renderBadOperation(String string) {
		return new AttributedStringBuilder()
				.append(string, AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW))
				.toAnsi();
	}

	@Override
	public String renderError(String string) {
		return new AttributedStringBuilder()
				.append(string, AttributedStyle.DEFAULT.foreground(AttributedStyle.RED))
				.toAnsi();
	}
}
