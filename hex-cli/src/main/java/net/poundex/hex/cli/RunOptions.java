package net.poundex.hex.cli;

import picocli.CommandLine;

import static picocli.CommandLine.Option.NULL_VALUE;

@CommandLine.Command(versionProvider = CliConfiguration.VersionProvider.class)
public interface RunOptions {
	@CommandLine.Option(names = {"-s", "--stack-size"}, description = "Visible stack size", defaultValue = "8")
	int getDisplayStackSize();
	
	@CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display help")
	boolean isHelpRequested();

	@CommandLine.Parameters(description = "input")
	String[] getInput();
	
	@CommandLine.Option(names = "--colour", negatable = true, defaultValue = NULL_VALUE)
	Boolean isColour();
	
	@CommandLine.Option(names = {"-V", "--version"}, versionHelp = true, description = "version information")
	boolean isVersionRequested();
}
