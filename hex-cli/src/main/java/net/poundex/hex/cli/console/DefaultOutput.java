package net.poundex.hex.cli.console;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
class DefaultOutput implements Output {
	
	private static final Logger out = LoggerFactory.getLogger("CLIOUT");
	private static final Logger err = LoggerFactory.getLogger("CLIERR");
	
	@Override
	public void out(String string, Object... args) {
		out.info(string, args);
	}

	@Override
	public void err(String string, Object... args) {
		err.info(string, args);
	}
}
