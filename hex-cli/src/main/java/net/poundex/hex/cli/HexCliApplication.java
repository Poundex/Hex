package net.poundex.hex.cli;

import net.poundex.hex.engine.HexContextConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportRuntimeHints;

@SpringBootApplication
@Import(HexContextConfiguration.class)
@ImportRuntimeHints(NativeHints.class)
public class HexCliApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(HexCliApplication.class, args);
	}
}
