package net.poundex.hex.cli.console;

public interface Output {
	void out(String string, Object... args);
	void err(String string, Object... args);
}
