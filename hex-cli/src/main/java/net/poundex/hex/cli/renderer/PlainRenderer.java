package net.poundex.hex.cli.renderer;

import org.springframework.stereotype.Component;

@Component
class PlainRenderer implements Renderer {
	@Override
	public String renderStackElement(int idx, String string) {
		return String.format("%s: %s", idx, string);
	}

	@Override
	public String renderText(String string) {
		return string;
	}

	@Override
	public String renderInfo(String string) {
		return string;
	}

	@Override
	public String renderBadOperation(String string) {
		return string;
	}

	@Override
	public String renderError(String string) {
		return string;
	}
}
