package net.poundex.hex.cli.session;

import lombok.RequiredArgsConstructor;
import net.poundex.hex.function.FunctionRegistry;
import net.poundex.hex.symbol.SymbolManager;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
class InteractiveCompleter implements Completer {
	
	private final SymbolManager symbolManager;
	private final FunctionRegistry functionRegistry;
	
	@Override
	public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
		symbolManager.getNames().stream()
			.map(n -> "$" + n)
			.forEach(sym -> candidates.add(new Candidate(sym)));
		
		functionRegistry.getNames().forEach(n -> candidates.add(new Candidate(n)));
	}
}
