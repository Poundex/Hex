package net.poundex.hex.cli.session;

import lombok.RequiredArgsConstructor;
import net.poundex.hex.cli.RunOptions;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.Collections;
import java.util.List;

@Component
@RequiredArgsConstructor
class SessionStarter implements ApplicationListener<ApplicationReadyEvent> {
	
	private final RunOptions runOptions;
	private final CommandLine commandLine;
	
	private final InteractiveSession interactiveSession;
	private final StaticSession staticSession;

	@SuppressWarnings("deprecation")
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		commandLine.setExecutionStrategy(new CommandLine.AbstractParseResultHandler<List<Object>>() {
			@Override
			protected List<Object> handle(CommandLine.ParseResult parseResult) throws CommandLine.ExecutionException {
				return Collections.emptyList();
			}

			@Override
			protected CommandLine.AbstractParseResultHandler<List<Object>> self() {
				return this;
			}
		});
		int execute = commandLine.execute(event.getArgs());// TODO - Args parsed twice
		if( execute != 0)
			System.exit(execute);
		
		if(runOptions.getInput() == null)
			interactiveSession.start();
		else
			staticSession.start();
	}
}
