package net.poundex.hex.symbol;

import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.NumericLiteral;
import net.poundex.hex.stack.StackElement;
import net.poundex.hex.stack.operation.BadOperationException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

@Service
class DefaultSymbolManager implements SymbolManager {
	
	private static final Map<String, StackElement> BUILT_IN_SYMBOLS = Map.of(
		"PI", new NumericLiteral(new BigDecimal(Double.toString(Math.PI))),
		"E", new NumericLiteral(new BigDecimal(Double.toString(Math.PI))));
	
	private final Map<String, StackElement> symbols = new HashMap<>();
	
	@Override
	public Optional<StackElement> findByName(String name, CalculatorContext calculatorContext) {
		return Optional.ofNullable(BUILT_IN_SYMBOLS.get(name.toUpperCase()))
			.or(() -> Stream.of(name).filter(n -> n.matches("^[0-9]+$"))
				.findFirst()
				.map(Integer::parseInt)
				.map(ref -> calculatorContext.getStack().getView().get(ref)))
			.or(() -> Optional.ofNullable(symbols.get(name)));
	}

	@Override
	public void set(String name, StackElement value) {
		if(BUILT_IN_SYMBOLS.containsKey(name.toUpperCase()))
			throw new BadOperationException("Can't overwrite built-in symbol %s".formatted(name));
		
		if(name.matches("^[0-9]+$"))
			throw new BadOperationException("Can't overwrite refs");
		
		symbols.put(name, value);
	}

	@Override
	public void unset(String name) {
		symbols.remove(name);
	}

	@Override
	public Set<String> getNames() {
		return symbols.keySet();
	}
}
