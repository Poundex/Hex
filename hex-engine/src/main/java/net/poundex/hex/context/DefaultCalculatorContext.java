package net.poundex.hex.context;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.HexException;
import net.poundex.hex.parser.StringParser;
import net.poundex.hex.stack.CalculatorStack;
import net.poundex.hex.stack.operation.Status;
import net.poundex.hex.symbol.SymbolManager;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Getter
@Component
class DefaultCalculatorContext implements CalculatorContext {
	
	private final CalculatorStack stack;
	private final SymbolManager symbolManager;
	@Getter(AccessLevel.NONE)
	private final StringParser stringParser;
	
	private Status status = Status.NONE;

	@Override
	public void parse(String input) {
		try {
			status = stringParser.parse(input).apply(this);
		} catch (Exception ex) {
			switch(ex) {
				case HexException hex -> status = hex.getStatus();
				default -> status = new Status(Status.MessageType.ERROR, ex.getClass().getSimpleName() + ": " + ex.getMessage());
			}
		}
	}
}
