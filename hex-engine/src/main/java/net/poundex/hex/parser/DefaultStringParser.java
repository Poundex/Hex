package net.poundex.hex.parser;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.function.FunctionRegistry;
import net.poundex.hex.stack.NumericLiteral;
import net.poundex.hex.stack.Symbol;
import net.poundex.hex.stack.operation.CopyRefOperation;
import net.poundex.hex.stack.operation.FunctionOperation;
import net.poundex.hex.stack.operation.PushOperation;
import net.poundex.hex.stack.operation.StackOperation;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Service
class DefaultStringParser implements StringParser {

	private static final Pattern REF_PATTERN = Pattern.compile("\\$([0-9]+)");
	private static final Pattern SYMBOL_PATTERN = Pattern.compile("\\$([a-zA-Z][a-zA-Z0-9_]*)");
	
	private final FunctionRegistry functionRegistry;
	
	@Override
	public ParseResult parse(String inputString) {
		return new ParseResult(
			Try.of(() -> Arrays.stream(inputString.split(" "))
					.map(this::doParse))
				.getOrElseThrow(x -> new InputParseException(inputString, x))
				.toList());
	}
	
	private StackOperation doParse(String token) {
		return findNumber(token)
			.or(() -> findRef(token))
			.or(() -> findSymbol(token))
			.or(() -> findFunction(token))
			.orElseThrow(() -> new TokenParseException(token));
	}

	private Optional<StackOperation> findNumber(String token) {
		return Try.of(() -> new BigDecimal(token))
			.map(NumericLiteral::new)
			.<StackOperation>map(PushOperation::new)
			.recover(NumberFormatException.class, (StackOperation) null)
			.toJavaOptional();
	}

	private Optional<StackOperation> findRef(String token) {
		Matcher matcher = REF_PATTERN.matcher(token);
		boolean found = matcher.find();

		if( ! found) return Optional.empty();
		return Optional.of(new CopyRefOperation(Integer.parseInt(matcher.group(1))));
	}

	private Optional<StackOperation> findSymbol(String token) {
		Matcher matcher = SYMBOL_PATTERN.matcher(token);
		boolean found = matcher.find();

		if( ! found) return Optional.empty();
		return Optional.of(new PushOperation(new Symbol(matcher.group(1))));
	}

	private Optional<StackOperation> findFunction(String token) {
		return functionRegistry.findByName(token)
			.map(FunctionOperation::new);
	}
}
