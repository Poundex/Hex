package net.poundex.hex.function;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
class DefaultFunctionRegistry implements FunctionRegistry {
	
	private final Map<String, FunctionDefinition> registry = new HashMap<>();
	
	@Override
	public Optional<FunctionDefinition> findByName(String name) {
		return Optional.ofNullable(registry.get(name));
	}

	@Override
	public void register(FunctionDefinition function) {
		registry.put(function.name(), function);
	}

	@Override
	public Set<String> getNames() {
		return registry.keySet();
	}
}
