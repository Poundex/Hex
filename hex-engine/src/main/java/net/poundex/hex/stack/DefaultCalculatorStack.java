package net.poundex.hex.stack;

import io.vavr.control.Try;
import net.poundex.hex.stack.operation.Status;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
class DefaultCalculatorStack implements CalculatorStack {
	
	private final LinkedList<StackElement> stack = new LinkedList<>();
	
	@Override
	public void push(StackElement stackElement) {
		stack.push(stackElement);
	}

	@Override
	public Status popAnd(Function<StackElement, Status> fn) {
		StackElement popped = stack.pop();
		return Try.of(() -> fn.apply(popped)).onFailure(_ -> stack.push(popped)).get();
	}

	@Override
	public Map<Integer, StackElement> getView() {
		return IntStream.range(0, stack.size())
			.boxed()
			.collect(Collectors.toMap(i -> i, stack::get));
	}

	@Override
	public int size() {
		return stack.size();
	}

	@Override
	public StackElement peek() {
		return stack.peek();
	}

	@Override
	public void clear() {
		stack.clear();
	}
}
