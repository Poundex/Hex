package net.poundex.hex.engine;

import net.poundex.hex.function.FunctionRegistry;
import net.poundex.hex.function.FunctionSource;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "net.poundex.hex")
@ComponentScan(basePackages = "net.poundex.hex", includeFilters = 
	@ComponentScan.Filter(
		type = FilterType.ASSIGNABLE_TYPE, 
		value = FunctionSource.class))
public class HexContextConfiguration {
	@Bean
	public ApplicationRunner runner(FunctionRegistry registry, ObjectProvider<FunctionSource> functionSources) {
		return ignored -> functionSources.stream()
			.flatMap(FunctionSource::getFunctions)
			.forEach(registry::register);
	}
}
