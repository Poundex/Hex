package net.poundex.hex.stack

import net.poundex.hex.stack.operation.Status
import spock.lang.Specification

import java.util.function.Function

class DefaultCalculatorStackSpec extends Specification {
	
	void "Returns empty view"() {
		given:
		CalculatorStack stack = new DefaultCalculatorStack()
		
		expect:
		stack.getView().isEmpty()
	}
	
	void "Pushes and pops elements from stack"() {
		given:
		CalculatorStack stack = new DefaultCalculatorStack()
		StackElement aStackElement = Stub(StackElement)
		Function<StackElement, Status> stackFn = Mock()
		Status aStatus = new Status(null, "stub-status")
		
		when:
		stack.push(aStackElement)
		
		then:
		with(stack.view) {
			size() == 1
			get(0) == aStackElement
		}

		when:
		Status r = stack.popAnd(stackFn)
		
		then:
		1 * stackFn.apply(aStackElement) >> aStatus
		stack.view.size() == 0
		r == aStatus
	}
	
	void "Returns correct view for populated stack"() {
		given:
		CalculatorStack stack = new DefaultCalculatorStack()
		StackElement stackElement1 = Stub(StackElement)
		StackElement stackElement2 = Stub(StackElement)
		Function<StackElement, Status> stackFn = Mock()
		Status aStatus = new Status(null, "stub-status")

		when:
		stack.push(stackElement1)
		stack.push(stackElement2)
		
		then:
		with(stack.view) {
			size() == 2
			get(0) == stackElement2
			get(1) == stackElement1
		}
		
		when:
		Status rOne = stack.popAnd(stackFn)
		Status rTwo = stack.popAnd(stackFn)
		
		then:
		1 * stackFn.apply(stackElement1) >> aStatus
		1 * stackFn.apply(stackElement2) >> aStatus
		rOne == aStatus
		rTwo == aStatus
	}
}
