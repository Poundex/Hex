package net.poundex.hex.symbol

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.stack.CalculatorStack
import net.poundex.hex.stack.NumericLiteral
import net.poundex.hex.stack.operation.BadOperationException
import spock.lang.Specification
import spock.lang.Subject

class DefaultSymbolManagerSpec extends Specification {
	
	@Subject
	SymbolManager symbolManager = new DefaultSymbolManager()
	
	void "Returns empty optional when not found"() {
		expect:
		symbolManager.findByName("something", null) == Optional.empty()
	}
	
	void "Sets symbol and returns"() {
		given:
		NumericLiteral num = new NumericLiteral(123.0g)
		
		when:
		symbolManager.set("foo", num)
		
		then:
		symbolManager.findByName("foo", null) == Optional.of(num)
	}
	
	void "Returns built-in symbol by name"() {
		expect:
		with(symbolManager.findByName("pi", null).get() as NumericLiteral) {
			it.getValue(null) == Math.PI.toBigDecimal()
		}
	}
	
	void "Returns stack elements by ref"() {
		given:
		CalculatorContext calculatorContext = Stub(CalculatorContext) {
			getStack() >> Stub(CalculatorStack) {
				getView() >> [
					0: new NumericLiteral(7g), 
					1: new NumericLiteral(8g), 
					2: new NumericLiteral(9g)]
			}
		}
		
		expect:
		with(symbolManager.findByName("1", calculatorContext).get() as NumericLiteral) {
			it.getValue(calculatorContext) == 8
		}
	}
	
	void "Can't set built-ins"() {
		when:
		symbolManager.set("pi", new NumericLiteral(3.0g))
		
		then:
		thrown(BadOperationException)
	}

	void "Can't set refs"() {
		when:
		symbolManager.set("0", new NumericLiteral(3.0g))

		then:
		thrown(BadOperationException)
	}
}
