package net.poundex.hex.parser

import net.poundex.hex.function.FunctionDefinition
import net.poundex.hex.function.FunctionRegistry
import net.poundex.hex.stack.NumericLiteral
import net.poundex.hex.stack.Symbol
import net.poundex.hex.stack.operation.CopyRefOperation
import net.poundex.hex.stack.operation.FunctionOperation
import net.poundex.hex.stack.operation.PushOperation
import spock.lang.Specification
import spock.lang.Subject

class DefaultStringParserSpec extends Specification {
	
	FunctionRegistry functionRegistry = Stub()
	
	@Subject
	StringParser stringParser = new DefaultStringParser(functionRegistry)
	
	void "Parses numbers"() {
		when:
		ParseResult result = stringParser.parse("1.234")
		
		then:
		with(result.stackOperations) {
			size() == 1
			first() == new PushOperation(new NumericLiteral(1.234g))
		}
	}
	
	void "Parses symbols"() {
		when:
		ParseResult result = stringParser.parse('$var')
		
		then:
		with(result.stackOperations) {
			size() == 1
			first() == new PushOperation(new Symbol("var"))
		}
	}
	
	void "Parses functions"() {
		given:
		FunctionDefinition function = new FunctionDefinition("func", 0, null)
		functionRegistry.findByName("func") >> Optional.of(function)
		
		when:
		ParseResult result = stringParser.parse("func")
		
		then:
		with(result.stackOperations) {
			size() == 1
			first() == new FunctionOperation(function)
		}
	}
	
	void "Handles multiple tokens"() {
		given:
		FunctionDefinition plus = new FunctionDefinition("+", 0, null)
		FunctionDefinition store = new FunctionDefinition("=", 0, null)
		functionRegistry.findByName("+") >> Optional.of(plus)
		functionRegistry.findByName("=") >> Optional.of(store)
		
		when:
		ParseResult result = stringParser.parse('1.0 2.0 + $result =')

		then:
		with(result.stackOperations) {
			size() == 5
			get(0) == new PushOperation(new NumericLiteral(1.0g))
			get(1) == new PushOperation(new NumericLiteral(2.0g))
			get(2) == new FunctionOperation(plus)
			get(3) == new PushOperation(new Symbol("result"))
			get(4) == new FunctionOperation(store)
		}
	}
	
	void "Parses refs"() {
		when:
		ParseResult result = stringParser.parse('$0')

		then:
		with(result.stackOperations) {
			size() == 1
			first() == new CopyRefOperation(0)
		}
	}
}
