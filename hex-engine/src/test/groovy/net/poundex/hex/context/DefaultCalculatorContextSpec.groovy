package net.poundex.hex.context


import net.poundex.hex.parser.ParseResult
import net.poundex.hex.parser.StringParser
import spock.lang.Specification

class DefaultCalculatorContextSpec extends Specification {

	void "Parses input and applies result"() {
		given:
		ParseResult parseResult = Mock()
		StringParser stringParser = Stub() {
			parse("input") >> parseResult
		}
		CalculatorContext calculatorContext = new DefaultCalculatorContext(null, null, stringParser)
		
		when:
		calculatorContext.parse("input")
		
		then:
		1 * parseResult.apply(calculatorContext)
	}
}
