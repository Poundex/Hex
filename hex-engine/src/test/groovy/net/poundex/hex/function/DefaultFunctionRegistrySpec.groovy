package net.poundex.hex.function


import spock.lang.Specification
import spock.lang.Subject

class DefaultFunctionRegistrySpec extends Specification {
	
	@Subject
	FunctionRegistry functionRegistry = new DefaultFunctionRegistry()
	
	void "Returns empty optional when not found"() {
		expect:
		functionRegistry.findByName("anything").isEmpty()
	}
	
	void "Returns function which has been registered"() {
		given:
		FunctionDefinition function = new FunctionDefinition("something", 0, null)
		functionRegistry.register(function)
		
		expect:
		functionRegistry.findByName("something").get() == function
	}
}
