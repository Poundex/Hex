package net.poundex.hex.calculator


import net.poundex.hex.symbol.SymbolManager
import spock.lang.Subject

class BasicSymbolOperatorsSpec extends AbstractOperatorsSpec {
	
	SymbolManager symbolManager = Mock()
	
	@Subject
	BasicSymbolOperators basicSymbolOperators = new BasicSymbolOperators()

	void setup() {
		calculatorContext.getSymbolManager() >> symbolManager
	}

	void "Returns stream containing all operators"() {
		expect:
		basicSymbolOperators.functions.count() == 3
	}

	void "Store"() {
		given:
		calculatorStack.push number(2)
		calculatorStack.push symbol("two")

		when:
		invoking(BasicSymbolOperators.STORE)

		then:
		1 * symbolManager.set("two", number(2))
		
		and:
		calculatorStack.view.isEmpty()
	}
	
	void "Expand"() {
		given:
		calculatorStack.push symbol("two")
		symbolManager.findByName("two", calculatorContext) >> Optional.of(number(2))

		when:
		invoking(BasicSymbolOperators.EXPAND)

		then:
		calculatorStack.view == [0: number(2)]
	}

	void "Store"() {
		given:
		calculatorStack.push symbol("two")

		when:
		invoking(BasicSymbolOperators.UNSET)

		then:
		1 * symbolManager.unset("two")

		and:
		calculatorStack.view.isEmpty()
	}
}
