package net.poundex.hex.calculator

import spock.lang.Subject

class BasicStackOperatorsSpec extends AbstractOperatorsSpec {
	@Subject
	BasicStackOperators basicStackOperators = new BasicStackOperators()

	void "Returns stream containing all operators"() {
		expect:
		basicStackOperators.functions.count() == 6
	}

	void "Drop"() {
		given:
		calculatorStack.push number(1)

		when:
		invoking(BasicStackOperators.DROP)

		then:
		1 * calculatorStack.popAnd(_)
	}

	void "Dup"() {
		given:
		calculatorStack.push number(1)

		when:
		invoking(BasicStackOperators.DUP)

		then:
		1 * calculatorStack.push(number(1))
	}

	void "Swap"() {
		given:
		calculatorStack.push number(1)
		calculatorStack.push number(2)

		when:
		invoking(BasicStackOperators.SWAP)

		then:
		calculatorStack.view == [0: number(1), 1: number(2)]
	}

	void "Clear"() {
		given:
		calculatorStack.push number(1)
		calculatorStack.push number(2)

		when:
		invoking(BasicStackOperators.CLEAR)

		then:
		calculatorStack.view.isEmpty()
	}

	void "Roll away"() {
		given:
		calculatorStack.push(number(1))
		calculatorStack.push(number(2))
		calculatorStack.push(number(3))

		when:
		invoking(BasicStackOperators.ROLL_AWAY)

		then:
		calculatorStack.view == [
			2: number(2),
			1: number(3),
			0: number(1)
		]
	}

	void "Roll toward"() {
		given:
		calculatorStack.push(number(1))
		calculatorStack.push(number(2))
		calculatorStack.push(number(3))

		when:
		invoking(BasicStackOperators.ROLL_TOWARD)

		then:
		calculatorStack.view == [
			2: number(3),
			1: number(1),
			0: number(2)
		]
	}
}
