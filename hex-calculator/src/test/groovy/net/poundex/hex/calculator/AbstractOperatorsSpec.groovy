package net.poundex.hex.calculator

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.function.FunctionDefinition
import net.poundex.hex.stack.CalculatorStack
import net.poundex.hex.stack.NumericLiteral
import net.poundex.hex.stack.StackElement
import net.poundex.hex.stack.Symbol
import net.poundex.hex.stack.operation.Status
import spock.lang.Specification

import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.IntStream

abstract class AbstractOperatorsSpec extends Specification {

	Deque<StackElement> _stack = new LinkedList<>()
	
	CalculatorStack calculatorStack = Mock() {
		popAnd(_ as Function<StackElement, Status>) >> { Function<StackElement, Status> fn -> 
			fn.apply(_stack.pop()) 
		}
		peek() >> { _stack.peek() }
		push(_ as StackElement) >> { StackElement elem -> _stack.push(elem) }
		clear() >> { _stack.clear() }
		getView() >> {
			IntStream.range(0, _stack.size())
				.boxed()
				.collect(Collectors.toMap(i -> i, _stack::get)) 
		}
	}
	
	CalculatorContext calculatorContext = Stub() {
		getStack() >> calculatorStack
	}
	
	protected NumericLiteral number(Number number) {
		return new NumericLiteral(number.toBigDecimal())
	}

	protected Status invoking(FunctionDefinition functionDefinition) {
		return functionDefinition.function().apply(calculatorContext)
	}
	
	protected Symbol symbol(String name) {
		return new Symbol(name)
	}
}
