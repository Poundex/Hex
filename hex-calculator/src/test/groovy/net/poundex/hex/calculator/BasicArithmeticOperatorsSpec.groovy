package net.poundex.hex.calculator


import spock.lang.Subject

class BasicArithmeticOperatorsSpec extends AbstractOperatorsSpec {
	
	@Subject
	BasicArithmeticOperators basicArithmeticOperators = new BasicArithmeticOperators()
	
	void "Returns stream containing all operators"() {
		expect:
		basicArithmeticOperators.functions.count() == 8
	}
	
	void "Plus"() {
		given:
		calculatorStack.push number(1)
		calculatorStack.push number(2)

		when:
		invoking(BasicArithmeticOperators.PLUS)

		then:
		1 * calculatorStack.push(number(3))
	}

	void "Minus"() {
		given:
		calculatorStack.push number(5)
		calculatorStack.push number(2)

		when:
		invoking(BasicArithmeticOperators.MINUS)

		then:
		1 * calculatorStack.push(number(3))

	}
	
	void "Multiply"() {
		given:
		calculatorStack.push number(5)
		calculatorStack.push number(2)

		when:
		invoking(BasicArithmeticOperators.MULTIPLY)

		then:
		1 * calculatorStack.push(number(10))
	}

	void "Divide"() {
		given:
		calculatorStack.push number(10)
		calculatorStack.push number(2)

		when:
		invoking(BasicArithmeticOperators.DIVIDE)

		then:
		1 * calculatorStack.push(number(5))
	}

	void "Modulo"() {
		given:
		calculatorStack.push number(10)
		calculatorStack.push number(9)

		when:
		invoking(BasicArithmeticOperators.MODULO)

		then:
		1 * calculatorStack.push(number(1))
	}

	void "Exp"() {
		given:
		calculatorStack.push number(3)
		calculatorStack.push number(2)

		when:
		invoking(BasicArithmeticOperators.EXP)

		then:
		1 * calculatorStack.push(number(9))
	}

	void "Sqrt"() {
		given:
		calculatorStack.push number(16)

		when:
		invoking(BasicArithmeticOperators.SQRT)

		then:
		1 * calculatorStack.push(number(4))
	}
}
