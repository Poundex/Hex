package net.poundex.hex.calculator;

import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.function.FunctionDefinition;
import net.poundex.hex.function.FunctionSource;
import net.poundex.hex.stack.StackElement;
import net.poundex.hex.stack.operation.Status;

import java.util.Map;
import java.util.stream.Stream;

class BasicStackOperators implements FunctionSource {
	
	static final FunctionDefinition DROP =
		new FunctionDefinition("<", 1, BasicStackOperators::drop);
	
	static final FunctionDefinition DUP = 
		new FunctionDefinition("", 1, BasicStackOperators::dup);
	
	static final FunctionDefinition SWAP = 
		new FunctionDefinition("~", 2, BasicStackOperators::swap);
	
	static final FunctionDefinition CLEAR =
		new FunctionDefinition("clr", 0, BasicStackOperators::clear);
	
	static final FunctionDefinition ROLL_AWAY = 
		new FunctionDefinition("<<", 0, BasicStackOperators::rollAway);
	
	static final FunctionDefinition ROLL_TOWARD =
		new FunctionDefinition(">>", 0, BasicStackOperators::rollToward);

	@Override
	public Stream<FunctionDefinition> getFunctions() {
		return Stream.of(DROP, DUP, SWAP, CLEAR, ROLL_AWAY, ROLL_TOWARD);
	}

	private static Status drop(CalculatorContext calculatorContext) {
		return calculatorContext.getStack().popAnd(ignored -> Status.NONE);
	}
	
	private static Status dup(CalculatorContext ctx) {
		ctx.getStack().push(ctx.getStack().peek());
		return Status.NONE;
	}
	
	private static Status swap(CalculatorContext ctx) {
		return ctx.getStack().popAnd(first ->
				ctx.getStack().popAnd(second -> {
					ctx.getStack().push(first);
					ctx.getStack().push(second);
					return Status.NONE;
				}));
	}

	private static Status clear(CalculatorContext ctx) {
		ctx.getStack().clear();
		return Status.NONE;
	}
	
	private static Status rollAway(CalculatorContext ctx) {
		Map<Integer, StackElement> view = ctx.getStack().getView();
		if (view.isEmpty()) return Status.NONE;

		int furthestIdx = view.keySet().stream().mapToInt(Integer::intValue).max().getAsInt();
		
		clear(ctx);
		for (int idx = furthestIdx - 1; idx >= 0; idx--) {
			ctx.getStack().push(view.get(idx));
		}
		ctx.getStack().push(view.get(furthestIdx));

		return Status.NONE;
	}
	
	private static Status rollToward(CalculatorContext ctx) {
		Map<Integer, StackElement> view = ctx.getStack().getView();
		if (view.isEmpty()) return Status.NONE;

		int furthestIdx = view.keySet().stream().mapToInt(Integer::intValue).max().getAsInt();

		clear(ctx);
		ctx.getStack().push(view.get(0));
		for (int idx = furthestIdx; idx >= 1; idx--) {
			ctx.getStack().push(view.get(idx));
		}

		return Status.NONE;
	}
}
