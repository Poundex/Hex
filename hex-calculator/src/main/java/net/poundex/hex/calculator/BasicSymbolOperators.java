package net.poundex.hex.calculator;

import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.function.FunctionDefinition;
import net.poundex.hex.function.FunctionSource;
import net.poundex.hex.stack.NumericLiteral;
import net.poundex.hex.stack.StackElement;
import net.poundex.hex.stack.Symbol;
import net.poundex.hex.stack.operation.BadOperationException;
import net.poundex.hex.stack.operation.Status;

import java.util.function.Function;
import java.util.stream.Stream;

class BasicSymbolOperators implements FunctionSource {
	
	static final FunctionDefinition STORE =
		new FunctionDefinition("=", 2, BasicSymbolOperators::store);
	
	static final FunctionDefinition EXPAND =
		new FunctionDefinition("?", 1, BasicSymbolOperators::expand);
	
	static final FunctionDefinition UNSET = 
		new FunctionDefinition("unset", 1, BasicSymbolOperators::unset);
	
	@Override
	public Stream<FunctionDefinition> getFunctions() {
		return Stream.of(STORE, EXPAND, UNSET);
	}

	private static Status store(CalculatorContext ctx) {
		return ctx.getStack().popAnd(elem -> withSymbol(elem, symbol ->
				ctx.getStack().popAnd(value -> {
					ctx.getSymbolManager().set(symbol.getName(), value);
					return new Status(Status.MessageType.INFO, String.format("$%s stored", symbol.getName()));
				})));
	}
	
	private static Status expand(CalculatorContext ctx) {
		return ctx.getStack().popAnd(head -> {
			ctx.getStack().push(new NumericLiteral(head.getValue(ctx)));
			return Status.NONE;
		});
	}
	
	private static Status unset(CalculatorContext ctx) {
		return ctx.getStack().popAnd(elem -> withSymbol(elem, symbol -> {
			ctx.getSymbolManager().unset(symbol.getName());
			return new Status(Status.MessageType.INFO, String.format("$%s unset", symbol.getName()));
		}));
	}
	
	private static <T> T withSymbol(StackElement stackElement, Function<Symbol, T> fn) {
		if( ! (stackElement instanceof Symbol))
			throw new BadOperationException("Symbol expected");
			
		return fn.apply((Symbol) stackElement);
	}
}
