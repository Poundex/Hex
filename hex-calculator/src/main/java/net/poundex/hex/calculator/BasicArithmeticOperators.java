package net.poundex.hex.calculator;

import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.function.FunctionDefinition;
import net.poundex.hex.function.FunctionSource;
import net.poundex.hex.stack.NumericLiteral;
import net.poundex.hex.stack.operation.Status;

import java.math.MathContext;
import java.util.stream.Stream;

class BasicArithmeticOperators implements FunctionSource {
	
	static final FunctionDefinition PLUS =
		new FunctionDefinition("+", 2, BasicArithmeticOperators::plus);
	
	static final FunctionDefinition MINUS = 
		new FunctionDefinition("-", 2, BasicArithmeticOperators::minus);
	
	static final FunctionDefinition MULTIPLY = 
		new FunctionDefinition("*", 2, BasicArithmeticOperators::multiply);
	static final FunctionDefinition MULTIPLY2 =
		new FunctionDefinition("x", 2, BasicArithmeticOperators::multiply);

	static final FunctionDefinition DIVIDE =
		new FunctionDefinition("/", 2, BasicArithmeticOperators::divide);
	
	static final FunctionDefinition MODULO =
		new FunctionDefinition("%", 2, BasicArithmeticOperators::modulo);

	static final FunctionDefinition EXP =
		new FunctionDefinition("^", 2, BasicArithmeticOperators::exp);

	static final FunctionDefinition SQRT =
		new FunctionDefinition("v", 1, BasicArithmeticOperators::sqrt);
	

	@Override
	public Stream<FunctionDefinition> getFunctions() {
		return Stream.of(PLUS, MINUS, MULTIPLY, MULTIPLY2, DIVIDE, MODULO, EXP, SQRT);
	}

	private static Status plus(CalculatorContext ctx) {
		return ctx.getStack().popAnd(addend ->
				ctx.getStack().popAnd(augend -> {
					ctx.getStack().push(new NumericLiteral(
							addend.getValue(ctx).add(augend.getValue(ctx))));
					return Status.NONE;
				}));
	}
	
	private static Status minus(CalculatorContext ctx) {
		return ctx.getStack().popAnd(subtrahend ->
				ctx.getStack().popAnd(minuend -> {
					ctx.getStack().push(new NumericLiteral(
							minuend.getValue(ctx).subtract(subtrahend.getValue(ctx))));
					return Status.NONE;
				}));
	}
	
	private static Status multiply(CalculatorContext ctx) {
		return ctx.getStack().popAnd(multiplier ->
				ctx.getStack().popAnd(multiplicand -> {
					ctx.getStack().push(new NumericLiteral(
							multiplier.getValue(ctx).multiply(multiplicand.getValue(ctx))));
					return Status.NONE;
				}));
	}

	private static Status divide(CalculatorContext ctx) {
		return ctx.getStack().popAnd(divisor ->
				ctx.getStack().popAnd(dividend -> {
					ctx.getStack().push(new NumericLiteral(
							dividend.getValue(ctx).divide(divisor.getValue(ctx), MathContext.DECIMAL64)));
					return Status.NONE;
				}));
	}

	private static Status modulo(CalculatorContext ctx) {
		return ctx.getStack().popAnd(divisor ->
				ctx.getStack().popAnd(dividend -> {
					ctx.getStack().push(new NumericLiteral(
							dividend.getValue(ctx).remainder(divisor.getValue(ctx))));
					return Status.NONE;
				}));
	}

	private static Status exp(CalculatorContext ctx) {
		return ctx.getStack().popAnd(exponent ->
				ctx.getStack().popAnd(base -> {
					ctx.getStack().push(new NumericLiteral(
							base.getValue(ctx).pow(exponent.getValue(ctx).intValueExact())));
					return Status.NONE;
				}));
	}
	
	private static Status sqrt(CalculatorContext ctx) {
		return ctx.getStack().popAnd(degree -> {
			ctx.getStack().push(new NumericLiteral(
					degree.getValue(ctx).sqrt(MathContext.DECIMAL128)));
			return Status.NONE;
		});
	}
}
