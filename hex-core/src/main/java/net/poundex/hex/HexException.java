package net.poundex.hex;

import lombok.experimental.StandardException;
import net.poundex.hex.stack.operation.Status;

@StandardException
public abstract class HexException extends RuntimeException { 
	public abstract Status getStatus();
}

