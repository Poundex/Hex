package net.poundex.hex.function;

import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.operation.Status;

import java.util.function.Function;

public record FunctionDefinition(
	String name, 
	int minStackSize, 
	Function<CalculatorContext, Status> function) {
}
