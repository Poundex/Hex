package net.poundex.hex.function;

import java.util.stream.Stream;

public interface FunctionSource {
	Stream<FunctionDefinition> getFunctions();
}
