package net.poundex.hex.function;

import java.util.Optional;
import java.util.Set;

public interface FunctionRegistry {
	Optional<FunctionDefinition> findByName(String name);
	void register(FunctionDefinition function);
	Set<String> getNames();
}
