package net.poundex.hex.symbol;

import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.StackElement;

import java.util.Optional;
import java.util.Set;

public interface SymbolManager {
	Optional<StackElement> findByName(String name, CalculatorContext calculatorContext);
	void set(String name, StackElement value);
	void unset(String name);
	Set<String> getNames();
}
