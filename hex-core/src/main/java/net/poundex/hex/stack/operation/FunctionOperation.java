package net.poundex.hex.stack.operation;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.function.FunctionDefinition;
import net.poundex.hex.stack.CalculatorStack;

@RequiredArgsConstructor
@EqualsAndHashCode
public class FunctionOperation implements StackOperation {
	private final FunctionDefinition function;

	@Override
	public Status apply(CalculatorContext calculatorContext) {
		assertStackLength(calculatorContext.getStack());
		return function.function().apply(calculatorContext);
	}

	private void assertStackLength(CalculatorStack stack) {
		int stackSize = stack.size();
		if(stackSize < function.minStackSize())
			throw new IllegalArgumentException(
				String.format("'%s' needs %s elements", function.name(), function.minStackSize()));
	}
}
