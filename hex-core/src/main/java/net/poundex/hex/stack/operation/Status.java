package net.poundex.hex.stack.operation;

public record Status(MessageType messageType, String message) {
	
	public static Status NONE = new Status(MessageType.NONE, "");
	
	public enum MessageType {
		NONE, INFO, BAD_OPERATION, ERROR
	}
}
