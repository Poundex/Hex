package net.poundex.hex.stack;

import net.poundex.hex.context.CalculatorContext;

import java.math.BigDecimal;

public interface StackElement {
	BigDecimal getValue(CalculatorContext calculatorContext);
	String getDisplay(CalculatorContext calculatorContext);
}
