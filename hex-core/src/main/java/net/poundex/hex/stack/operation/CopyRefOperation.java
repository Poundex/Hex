package net.poundex.hex.stack.operation;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.context.CalculatorContext;

@RequiredArgsConstructor
@Data
public class CopyRefOperation implements StackOperation {
	
	private final int ref;

	@Override
	public Status apply(CalculatorContext calculatorContext) {
		if( ! calculatorContext.getStack().getView().containsKey(ref))
			throw new BadOperationException("Nothing in stack @ " + ref);
		
		calculatorContext.getStack().push(
			calculatorContext.getStack().getView().get(ref));
		
		return Status.NONE;
	}
}
