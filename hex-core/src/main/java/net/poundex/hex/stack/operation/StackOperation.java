package net.poundex.hex.stack.operation;

import net.poundex.hex.context.CalculatorContext;

public interface StackOperation {
	Status apply(CalculatorContext calculatorContext);
}
