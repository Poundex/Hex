package net.poundex.hex.stack;

import net.poundex.hex.stack.operation.Status;

import java.util.Map;
import java.util.function.Function;

public interface CalculatorStack {
	void push(StackElement stackElement);
	Status popAnd(Function<StackElement, Status> fn);
	StackElement peek();

	int size();

	Map<Integer, StackElement> getView();

	void clear();
}
