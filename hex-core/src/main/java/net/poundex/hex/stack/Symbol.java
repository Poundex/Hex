package net.poundex.hex.stack;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.operation.BadOperationException;

import java.math.BigDecimal;

@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class Symbol implements StackElement {
	@Getter
	private final String name;

	@Override
	public BigDecimal getValue(CalculatorContext calculatorContext) {
		return calculatorContext.getSymbolManager()
			.findByName(name, calculatorContext)
			.orElseThrow(() -> new BadOperationException(String.format("$%s is undefined", name)))
			.getValue(calculatorContext);
	}

	@Override
	public String getDisplay(CalculatorContext calculatorContext) {
		return "$" + name;
	}
}
