package net.poundex.hex.stack.operation;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.StackElement;

@RequiredArgsConstructor
@Data
public class PushOperation implements StackOperation {
	
	private final StackElement stackElement;

	@Override
	public Status apply(CalculatorContext calculatorContext) {
		calculatorContext.getStack().push(stackElement);
		return Status.NONE;
	}
}
