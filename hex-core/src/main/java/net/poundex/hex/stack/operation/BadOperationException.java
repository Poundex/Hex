package net.poundex.hex.stack.operation;

import net.poundex.hex.HexException;

public class BadOperationException extends HexException {
	public BadOperationException(String message) {
		super(message);
	}

	public BadOperationException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public Status getStatus() {
		return new Status(Status.MessageType.BAD_OPERATION, getMessage());
	}
}
