package net.poundex.hex.stack;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.poundex.hex.context.CalculatorContext;

import java.math.BigDecimal;

@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class NumericLiteral implements StackElement {
	private final BigDecimal number;

	@Override
	public BigDecimal getValue(CalculatorContext calculatorContext) {
		return number;
	}

	@Override
	public String getDisplay(CalculatorContext calculatorContext) {
		return number.toString();
	}
}
