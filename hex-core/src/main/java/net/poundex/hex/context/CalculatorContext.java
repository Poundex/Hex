package net.poundex.hex.context;

import net.poundex.hex.stack.CalculatorStack;
import net.poundex.hex.stack.operation.Status;
import net.poundex.hex.symbol.SymbolManager;

public interface CalculatorContext {
	CalculatorStack getStack();
	SymbolManager getSymbolManager();
	void parse(String input);
	Status getStatus();
}
