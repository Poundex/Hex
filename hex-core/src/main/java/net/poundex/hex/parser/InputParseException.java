package net.poundex.hex.parser;

public class InputParseException extends RuntimeException {
	public InputParseException(String inputString, Throwable cause) {
		super("For input string: " + inputString, cause);
	}
}
