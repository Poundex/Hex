package net.poundex.hex.parser;

import net.poundex.hex.stack.operation.BadOperationException;

public class TokenParseException extends BadOperationException {
	public TokenParseException(String token) {
		super(String.format("Invalid token: '%s'", token));
	}
}
