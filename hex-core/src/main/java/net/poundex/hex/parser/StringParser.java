package net.poundex.hex.parser;

public interface StringParser {
	ParseResult parse(String inputString);
}
