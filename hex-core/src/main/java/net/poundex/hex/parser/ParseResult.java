package net.poundex.hex.parser;

import io.vavr.collection.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.poundex.hex.context.CalculatorContext;
import net.poundex.hex.stack.operation.StackOperation;
import net.poundex.hex.stack.operation.Status;

import java.util.List;

@RequiredArgsConstructor
public class ParseResult {

	@Getter
	private final List<StackOperation> stackOperations;

	public Status apply(CalculatorContext calculatorContext) {
		return Stream.ofAll(stackOperations)
			.foldLeft(Status.NONE,
				(_, op) -> op.apply(calculatorContext));
	}
}
