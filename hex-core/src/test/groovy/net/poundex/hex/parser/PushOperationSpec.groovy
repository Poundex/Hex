package net.poundex.hex.parser

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.stack.CalculatorStack
import net.poundex.hex.stack.NumericLiteral
import net.poundex.hex.stack.operation.PushOperation
import spock.lang.Specification

class PushOperationSpec extends Specification {
	void "Pushes StackElement to stack"() {
		given:
		CalculatorStack calculatorStack = Mock()
		CalculatorContext calculatorContext = Stub() {
			getStack() >> calculatorStack
		}
		NumericLiteral literal = new NumericLiteral(1.23g)
		PushOperation operation = new PushOperation(literal)
		
		when:
		operation.apply(calculatorContext)
		
		then:
		1 * calculatorStack.push(literal)
	}
}
