package net.poundex.hex.parser

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.stack.CalculatorStack
import net.poundex.hex.stack.StackElement
import net.poundex.hex.stack.operation.PushOperation
import spock.lang.Specification

class ParseResultSpec extends Specification {
	
	void "Applies parsed operations to stack"() {
		given:
		StackElement stackElement1 = Stub()
		StackElement stackElement2 = Stub()
		StackElement stackElement3 = Stub()
		ParseResult r = new ParseResult([stackElement1, stackElement2, stackElement3].collect { new PushOperation(it) })
		CalculatorStack calculatorStack = Mock()
		CalculatorContext calculatorContext = Stub() {
			getStack() >> calculatorStack
		}

		when:
		r.apply(calculatorContext)

		then:
		1 * calculatorStack.push(stackElement1)
		1 * calculatorStack.push(stackElement2)
		1 * calculatorStack.push(stackElement3)
	}
}
