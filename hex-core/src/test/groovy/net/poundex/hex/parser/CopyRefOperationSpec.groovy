package net.poundex.hex.parser

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.stack.CalculatorStack
import net.poundex.hex.stack.NumericLiteral
import net.poundex.hex.stack.operation.CopyRefOperation
import spock.lang.Specification

class CopyRefOperationSpec extends Specification {
	void "Copies ref and pushes"() {
		given:
		CalculatorStack calculatorStack = Mock() {
			getView() >> [0: new NumericLiteral(1.23g)]
		}
		CalculatorContext calculatorContext = Stub() {
			getStack() >> calculatorStack
		}
		NumericLiteral literal = new NumericLiteral(1.23g)
		CopyRefOperation operation = new CopyRefOperation(0)
		
		when:
		operation.apply(calculatorContext)
		
		then:
		1 * calculatorStack.push(literal)
	}
}
