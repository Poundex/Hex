package net.poundex.hex.parser

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.function.FunctionDefinition
import net.poundex.hex.stack.CalculatorStack
import net.poundex.hex.stack.operation.FunctionOperation
import net.poundex.hex.stack.operation.Status
import spock.lang.Specification

import java.util.function.Function

class FunctionOperationSpec extends Specification {
	void "Runs function"() {
		given:
		Function<CalculatorContext, Status> function = Mock()
		FunctionDefinition functionDefinition = new FunctionDefinition(null, 0, function)
		FunctionOperation operation = new FunctionOperation(functionDefinition)
		CalculatorStack calculatorStack = Stub()
		CalculatorContext calculatorContext = Stub() {
			getStack() >> calculatorStack
		}
		
		when:
		operation.apply(calculatorContext)
		
		then:
		1 * function.apply(calculatorContext)
	}
}
