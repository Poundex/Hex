package net.poundex.hex.stack

import net.poundex.hex.context.CalculatorContext
import net.poundex.hex.stack.operation.BadOperationException
import net.poundex.hex.symbol.SymbolManager
import spock.lang.Specification
import spock.lang.Subject

class SymbolSpec extends Specification {
	
	SymbolManager symbolManager = Stub()
	CalculatorContext calculatorContext = Stub() {
		getSymbolManager() >> symbolManager
	}
	
	@Subject
	Symbol symbol = new Symbol("name")
	
	void "Throws when symbol has no value"() {
		when:
		symbol.getValue(calculatorContext)
		
		then:
		thrown(BadOperationException)
	}
	
	void "Returns stackelement for symbol value"() {
		given:
		symbolManager.findByName("name", calculatorContext) >> Optional.of(new NumericLiteral(1.23g))
		
		expect:
		symbol.getValue(calculatorContext) == 1.23g
	}
}
